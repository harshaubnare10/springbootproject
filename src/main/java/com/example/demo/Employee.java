package com.example.demo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.*;

@Entity
@Table(name="employee")

public class Employee {
    @Id

    private String name;

    private String age;

    private String email;

    private String pass;

    private String confirmpass;

    private String address;
    public void setName(String name)
    {
        this.name=name;
    }
    public void setAge(String age)
    {
        this.age=age;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }
    public void setPass(String pass)
    {
        this.pass=pass;
    }
    public void setConfirmpass(String confirmpass)
    {
        this.confirmpass=confirmpass;
    }
    public String getName()
    {
        return name;
    }
    public void setAddress(String address)
    {
        this.address=address;
    }
    public String getAge()
    {
        return age;
    }
    public String getEmail()
    {
        return email;
    }
    public String getAddress()
    {
        return address;
    }
    public Employee(String name,String age,String email,String pass,String address)
    {
        super();
        this.name=name;
        this.age=age;
        this.email=email;
        this.pass=pass;
        this.address=address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", email='" + email + '\'' +
                ", pass='" + pass + '\'' +
                ", confirmpass='" + confirmpass + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}