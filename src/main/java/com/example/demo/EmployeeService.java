package com.example.demo;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
@Service
@Transactional
public class EmployeeService {
    private final EmployeeRepository employeerepo;
    public EmployeeService(EmployeeRepository employeerepo)
    {
        this.employeerepo=employeerepo;
    }
    public void saveMyEmployeee(Employee employ)
    {
        employeerepo.save(employ);
    }
}
